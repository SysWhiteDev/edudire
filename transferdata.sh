#!/bin/bash

# Path to the folder you want to upload
folder_path="./PhoneData"

# Function to upload the folder to an FTP server
upload_folder() {
  local server="$1"

  # Connect to FTP server and upload the folder
  echo -e "\e[34mConnected: $server\e[0m"
  ncftpput -R -u anonymous -p anonymous -P 2121 "$server" / "$folder_path" > /dev/null
  echo -e "\e[32mDone: $server\e[0m"
}

# Continuous scanning loop
while true; do
  # Scan for FTP servers on the network
  ftp_servers=($(nmap -p 2121 --open -oG - 192.168.30.0/24 | awk '/2121\/open/{print $2}'))

  for server in "${ftp_servers[@]}"; do
    upload_folder "$server" &
  done

  # Wait for all background processes to finish
  wait

done